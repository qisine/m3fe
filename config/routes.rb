M3fe::Application.routes.draw do
  namespace :public do
    resources :home, only: :index
    resources :job_wanted_ads, only: [:index, :show, :create] 
    resources :job_offered_ads, only: [:index, :show, :create]
  end

  namespace :users do
    resources :job_wanted_ads
    resources :job_offered_ads
  end

  root :to => 'public/home#index'
end
