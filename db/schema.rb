# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130122032117) do

  create_table "ads", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.string   "phone"
    t.string   "advertisable_type"
    t.integer  "advertisable_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "code"
    t.string   "locale_index"
    t.integer  "categorizable_index"
    t.string   "categorizable_type"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "categories", ["categorizable_index"], :name => "index_categories_on_categorizable_index"
  add_index "categories", ["categorizable_type"], :name => "index_categories_on_categorizable_type"

  create_table "job_offered_ads", :force => true do |t|
    t.integer  "user_id"
    t.string   "work_place"
    t.integer  "pay"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "published",  :default => false
  end

  add_index "job_offered_ads", ["user_id"], :name => "index_job_offered_ads_on_user_id"

  create_table "job_wanted_ads", :force => true do |t|
    t.integer  "user_id"
    t.string   "desired_location"
    t.string   "attachment"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "published",        :default => false
  end

  add_index "job_wanted_ads", ["user_id"], :name => "index_job_wanted_ads_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "checksum_id"
    t.string   "default_locale"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

end
