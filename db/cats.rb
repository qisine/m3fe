Category.all { |c| c.destroy }

{
  EAT: "hotel_and_restaurant",
  TRANS: "translation",
  TEACH: "tutoring",
  TOUR: "tourism",
  OTHER: "other"
}.each do |k,v|
  Category.create!(code: k, locale_index: v)
end

puts "Categories created!"
