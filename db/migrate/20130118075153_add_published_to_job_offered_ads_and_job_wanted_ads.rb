class AddPublishedToJobOfferedAdsAndJobWantedAds < ActiveRecord::Migration
  def change
    add_column :job_offered_ads, :published, :boolean, default: false
    add_column :job_wanted_ads, :published, :boolean, default: false
  end
end
