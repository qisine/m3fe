class RenameHashIdToChecksumIdOnUser < ActiveRecord::Migration
  def change
    rename_column :users, :hash_id, :checksum_id
  end
end
