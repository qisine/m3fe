class RemoveEmailAndNameFromAds < ActiveRecord::Migration
  def change
    remove_column :job_offered_ads, :name
    remove_column :job_wanted_ads, :name
    remove_column :job_wanted_ads, :email
  end
end
