class CreateJobWantedAds < ActiveRecord::Migration
  def change
    create_table :job_wanted_ads do |t|
      t.references :user
      t.string :phone
      t.string :desired_location
      t.string :attachment
      t.string :title
      t.text :body

      t.timestamps
    end
    add_index :job_wanted_ads, :user_id
  end
end
