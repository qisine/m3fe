class RemoveBaseAttrsFromJobAds < ActiveRecord::Migration
  def up
    remove_column :job_offered_ads, :body, :phone, :title
    remove_column :job_wanted_ads, :body, :phone, :title
  end

  def down
    add_column :job_offered_ads, :body, :string
    add_column :job_offered_ads, :phone, :string
    add_column :job_offered_ads, :title, :string

    add_column :job_wanted_ads, :body, :string
    add_column :job_wanted_ads, :phone, :string
    add_column :job_wanted_ads, :title, :string
  end
end
