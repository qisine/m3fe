class AddEmailAndNameToJobWantedAds < ActiveRecord::Migration
  def change
    add_column :job_wanted_ads, :name, :string
    add_column :job_wanted_ads, :email, :string
    add_index :job_wanted_ads, :email
  end
end
