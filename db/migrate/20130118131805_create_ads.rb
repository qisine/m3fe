class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string :title
      t.text :body
      t.string :phone
      t.string :advertisable_type
      t.integer :advertisable_id

      t.timestamps
    end
  end
end
