class CreateJobOfferedAds < ActiveRecord::Migration
  def change
    create_table :job_offered_ads do |t|
      t.references :user
      t.string :name
      t.string :phone
      t.string :work_place
      t.integer :pay
      t.string :title
      t.text :body

      t.timestamps
    end
    add_index :job_offered_ads, :user_id
  end
end
