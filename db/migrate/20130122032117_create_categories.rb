class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :code
      t.string :locale_index
      t.integer :categorizable_index
      t.string :categorizable_type

      t.timestamps
    end

    add_index :categories, :categorizable_index
    add_index :categories, :categorizable_type
  end
end
