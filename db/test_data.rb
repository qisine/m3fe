unless Object.const_defined?('FactoryGirl')
  require 'factory_girl'
  require 'faker'
  require Rails.root.join('spec', 'factories.rb')
end

FactoryGirl.create_list(:user, 30)

puts "Test data created!"
  
