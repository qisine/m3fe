require 'spec_helper'

describe "job_wanted_ads/show" do
  before(:each) do
    @job_wanted_ad = assign(:job_wanted_ad, stub_model(JobWantedAd,
      :user => nil,
      :phone => "Phone",
      :desired_location => "Desired Location",
      :attachments => "Attachments",
      :title => "Title",
      :body => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Phone/)
    rendered.should match(/Desired Location/)
    rendered.should match(/Attachments/)
    rendered.should match(/Title/)
    rendered.should match(/MyText/)
  end
end
