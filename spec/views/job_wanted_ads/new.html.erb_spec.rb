require 'spec_helper'

describe "job_wanted_ads/new" do
  before(:each) do
    assign(:job_wanted_ad, stub_model(JobWantedAd,
      :user => nil,
      :phone => "MyString",
      :desired_location => "MyString",
      :attachments => "MyString",
      :title => "MyString",
      :body => "MyText"
    ).as_new_record)
  end

  it "renders new job_wanted_ad form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => job_wanted_ads_path, :method => "post" do
      assert_select "input#job_wanted_ad_user", :name => "job_wanted_ad[user]"
      assert_select "input#job_wanted_ad_phone", :name => "job_wanted_ad[phone]"
      assert_select "input#job_wanted_ad_desired_location", :name => "job_wanted_ad[desired_location]"
      assert_select "input#job_wanted_ad_attachments", :name => "job_wanted_ad[attachments]"
      assert_select "input#job_wanted_ad_title", :name => "job_wanted_ad[title]"
      assert_select "textarea#job_wanted_ad_body", :name => "job_wanted_ad[body]"
    end
  end
end
