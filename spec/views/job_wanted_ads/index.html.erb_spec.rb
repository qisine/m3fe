require 'spec_helper'

describe "job_wanted_ads/index" do
  before(:each) do
    assign(:job_wanted_ads, [
      stub_model(JobWantedAd,
        :user => nil,
        :phone => "Phone",
        :desired_location => "Desired Location",
        :attachments => "Attachments",
        :title => "Title",
        :body => "MyText"
      ),
      stub_model(JobWantedAd,
        :user => nil,
        :phone => "Phone",
        :desired_location => "Desired Location",
        :attachments => "Attachments",
        :title => "Title",
        :body => "MyText"
      )
    ])
  end

  it "renders a list of job_wanted_ads" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Desired Location".to_s, :count => 2
    assert_select "tr>td", :text => "Attachments".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
