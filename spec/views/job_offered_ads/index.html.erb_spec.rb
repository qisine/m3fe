require 'spec_helper'

describe "job_offered_ads/index" do
  before(:each) do
    assign(:job_offered_ads, [
      stub_model(JobOfferedAd,
        :user => nil,
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :work_place => "Work Place",
        :pay => 1,
        :title => "Title",
        :body => "MyText"
      ),
      stub_model(JobOfferedAd,
        :user => nil,
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :work_place => "Work Place",
        :pay => 1,
        :title => "Title",
        :body => "MyText"
      )
    ])
  end

  it "renders a list of job_offered_ads" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Work Place".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
