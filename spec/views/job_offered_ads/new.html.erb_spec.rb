require 'spec_helper'

describe "job_offered_ads/new" do
  before(:each) do
    assign(:job_offered_ad, stub_model(JobOfferedAd,
      :user => nil,
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :work_place => "MyString",
      :pay => 1,
      :title => "MyString",
      :body => "MyText"
    ).as_new_record)
  end

  it "renders new job_offered_ad form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => job_offered_ads_path, :method => "post" do
      assert_select "input#job_offered_ad_user", :name => "job_offered_ad[user]"
      assert_select "input#job_offered_ad_name", :name => "job_offered_ad[name]"
      assert_select "input#job_offered_ad_email", :name => "job_offered_ad[email]"
      assert_select "input#job_offered_ad_phone", :name => "job_offered_ad[phone]"
      assert_select "input#job_offered_ad_work_place", :name => "job_offered_ad[work_place]"
      assert_select "input#job_offered_ad_pay", :name => "job_offered_ad[pay]"
      assert_select "input#job_offered_ad_title", :name => "job_offered_ad[title]"
      assert_select "textarea#job_offered_ad_body", :name => "job_offered_ad[body]"
    end
  end
end
