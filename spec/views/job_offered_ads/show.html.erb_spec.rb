require 'spec_helper'

describe "job_offered_ads/show" do
  before(:each) do
    @job_offered_ad = assign(:job_offered_ad, stub_model(JobOfferedAd,
      :user => nil,
      :name => "Name",
      :email => "Email",
      :phone => "Phone",
      :work_place => "Work Place",
      :pay => 1,
      :title => "Title",
      :body => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/Email/)
    rendered.should match(/Phone/)
    rendered.should match(/Work Place/)
    rendered.should match(/1/)
    rendered.should match(/Title/)
    rendered.should match(/MyText/)
  end
end
