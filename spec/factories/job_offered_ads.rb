FactoryGirl.define do
  factory :job_offered_ad do
    association :base_ad, factory: :ad

    pay { [Random.new.rand(1..9999), nil].sample }
    work_place { [Faker::Address.city, nil].sample }
  end
end
