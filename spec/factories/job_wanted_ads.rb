FactoryGirl.define do
  factory :job_wanted_ad do
    association :base_ad, factory: :ad

    desired_location{ [Faker::Address.city, nil].sample }
    attachment { AttachmentFixtures.pick }
  end
end
