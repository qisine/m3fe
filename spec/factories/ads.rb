FactoryGirl.define do
  factory :ad do
    phone { [Faker::PhoneNumber.phone_number, nil].sample }
    title { Faker::Lorem.sentence(Random.new.rand(3..6)) }
    body { Faker::Lorem.paragraphs(Random.new.rand(1..5)).join("\n") }
  end
end
