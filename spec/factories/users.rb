FactoryGirl.define do
  factory :user do
    email

    name { Faker::Name.name }
    default_locale { ["en", "zh"].sample }
    
    after(:create) do |user, evaluator|
      if [true, false].sample
        FactoryGirl.create_list(:job_offered_ad, Random.new.rand(1..5), user: user)
      else
        FactoryGirl.create_list(:job_wanted_ad, Random.new.rand(1..3), user: user)
      end
    end
  end
end
