require "spec_helper"

describe JobOfferedAdsController do
  describe "routing" do

    it "routes to #index" do
      get("/job_offered_ads").should route_to("job_offered_ads#index")
    end

    it "routes to #new" do
      get("/job_offered_ads/new").should route_to("job_offered_ads#new")
    end

    it "routes to #show" do
      get("/job_offered_ads/1").should route_to("job_offered_ads#show", :id => "1")
    end

    it "routes to #edit" do
      get("/job_offered_ads/1/edit").should route_to("job_offered_ads#edit", :id => "1")
    end

    it "routes to #create" do
      post("/job_offered_ads").should route_to("job_offered_ads#create")
    end

    it "routes to #update" do
      put("/job_offered_ads/1").should route_to("job_offered_ads#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/job_offered_ads/1").should route_to("job_offered_ads#destroy", :id => "1")
    end

  end
end
