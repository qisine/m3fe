require "spec_helper"

describe JobWantedAdsController do
  describe "routing" do

    it "routes to #index" do
      get("/job_wanted_ads").should route_to("job_wanted_ads#index")
    end

    it "routes to #new" do
      get("/job_wanted_ads/new").should route_to("job_wanted_ads#new")
    end

    it "routes to #show" do
      get("/job_wanted_ads/1").should route_to("job_wanted_ads#show", :id => "1")
    end

    it "routes to #edit" do
      get("/job_wanted_ads/1/edit").should route_to("job_wanted_ads#edit", :id => "1")
    end

    it "routes to #create" do
      post("/job_wanted_ads").should route_to("job_wanted_ads#create")
    end

    it "routes to #update" do
      put("/job_wanted_ads/1").should route_to("job_wanted_ads#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/job_wanted_ads/1").should route_to("job_wanted_ads#destroy", :id => "1")
    end

  end
end
