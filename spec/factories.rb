class AttachmentFixtures
  def self.pick
    File.open(Dir.glob(File.join(Rails.root, "spec", "fixtures", "job_wanted_ads", "*")).sample)
  end
end

FactoryGirl.define do
  sequence(:email){|n| "#{Faker::Internet.user_name}#{n}@example.com"}
end

