module AdExtensions
  def self.included(base)
    base.extend ClassMethods
    base.class_eval do
      scope :page, ->(pg) {
        pg = pg.to_i rescue 0
        pp = Settings.items_per_page
        start_p = 0
        pages_count = (count.to_f/pp).ceil
        start_p = pp * (pg - 1) if(pg > 1 && pg <= pages_count)
        order("created_at DESC").offset(start_p).limit(pp)
      }

      scope :search, ->(kwd) {
        return scoped if kwd.empty?

        cols = ["ads.title", "ads.body"]
        cols << self::AD_CONF[:include] if self::AD_CONF[:include].present?
        cols.flatten!
        joins(:base_ad).where(cols.join(" ILIKE ? OR ") + " ILIKE ?", *(["%#{kwd}%"]*cols.size))
      }
    end
  end

  module ClassMethods
    def ad(opts = {});self.const_set("AD_CONF", opts);end
  end
end
