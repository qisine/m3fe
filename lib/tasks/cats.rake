namespace :db do
  desc "Create category records"
  task :cats => :seed do
    f = File.join(Rails.root, "db", "cats.rb")
    load(f) if File.exist?(f)
  end
end
