namespace :db do
  desc "Create test data"
  task :test_data => :seed do
    f = File.join(Rails.root, "db", "test_data.rb")
    load(f) if File.exist?(f)
  end
end
