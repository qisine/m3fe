#= require_self
#= require ./mixin
#= require_tree ./templates
#= require ./views/paginatable
#= require ./collections/paginatable
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers

@M3fe =
  Views: {}
  Routers: {}
  Collections: {}
  Models: {}
  init: ->
    new M3fe.Routers.Jobs
    Backbone.history.start()
  dispatcher: _.extend({}, Backbone.Events)

class @M3fe.ViewManager
  constructor: ->
    @views = {}

  insertAndRenderView: (newView) ->
    oldView = @views[newView.viewType]
    if oldView
      oldView.close() 

    newView.render()
    newView.insertIntoDOM()
     
    @views[newView.viewType] = newView

@M3fe.Views.viewMgr = new @M3fe.ViewManager

Backbone.View.prototype.close = ->
  @remove()
  @off()
  @onClose() if @onClose
