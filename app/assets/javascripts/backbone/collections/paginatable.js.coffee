M3fe.Collections.Paginatable =
  methodsToReplace: ["fetch", "parse", "pageInfo", "nextPage", "previousPage", "goTo"]
  initialize: ->
    #_.bindAll(this, "parse", "fetch", "pageInfo", "nextPage", "previousPage")
    @_mixedin_.page = 1
    @_mixedin_.perPage = (@_mixedin_.perPage || 25)

  fetch: (options) ->
    @trigger("fetching")

    options = (options || {})
    options.data = (options.data || {})
    _.defaults(options.data, @_mixedin_._pag_params, { page: @_mixedin_.page, perPage: @_mixedin_.perPage }) 

    that = this
    if options.success
      success = options.success
      options.success = (response) ->
        that.trigger("fetched")
        success(response)
    return Backbone.Collection.prototype.fetch.call(this, options) 

  parse: (response, xhr) ->
    @_mixedin_.page = parseInt(response.page, 10) 
    @_mixedin_.total = response.total
    @_mixedin_.perPage = response.perPage
    return response.models 

  pageInfo: ->
    info =
      total: @_mixedin_.total
      page: @_mixedin_.page
      perPage: @_mixedin_.perPage
      pages: Math.ceil(@_mixedin_.total / @_mixedin_.perPage)
      prev: 0
      next: 0

    info.prev = @_mixedin_.page - 1 if @_mixedin_.page > 1
    info.next = @_mixedin_.page + 1 if @_mixedin_.page < info.pages

    return info

  goTo: (page) ->
    return if page > @pageInfo().pages + 1 || page < 1
    @_mixedin_.page = page

    return this.fetch()

  nextPage: ->
    @_mixedin_.goTo(@_mixedin_.page + 1)

  previousPage: ->
    @_mixedin_.goTo(@_mixedin_.page - 1)
