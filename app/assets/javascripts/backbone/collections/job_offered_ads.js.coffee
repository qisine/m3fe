M3fe.Collections.JobOfferedAds = Backbone.Collection.extend
  model: M3fe.Models.JobOfferedAd
  url: '/public/job_offered_ads'

  initialize: ->
    @_mixedin_._bind_(@)
    @_mixedin_.initialize()
     
  goTo: (page)->
    @_mixedin_.goTo(page)

M3fe.Collections.JobOfferedAds.mixin(M3fe.Collections.Paginatable)
