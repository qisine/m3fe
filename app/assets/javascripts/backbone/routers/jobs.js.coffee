M3fe.Routers.Jobs = Backbone.Router.extend
  routes: 
    "": "offeredJobs"
    "offered": "offeredJobs"
    "offered/new": "addOfferedJob"
    "offered/:id": "showOfferedJob"
    "offered/:id/edit": "editOfferedJob"
    "offered/:id/update": "updateOfferedJob"
    "offered/:id/delete": "deleteOfferedJob"
    "wanted": "wantedJobs"
    "wanted/new": "addWantedJob"

  initialize: ->

  navigation: ->
    M3fe.Views.viewMgr.insertAndRenderView(new M3fe.Views.AdNavigation)

  offeredJobs: ->
    @navigation()
    M3fe.Views.viewMgr.insertAndRenderView(new M3fe.Views.JobOfferedAds)

  addOfferedJob: ->

  showOfferedJob: ->

  editOfferedJob: ->

  updateOfferedJob: ->

  deleteOfferedJob: ->
