M3fe.Models.JobOfferedAd = Backbone.Model.extend
  url: ->
    base = '/job_offered_ads'
    @isNew() ? base : base + '/' + @id
