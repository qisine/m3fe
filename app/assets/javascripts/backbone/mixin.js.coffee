M3fe.mixin = (src) ->
  target = @prototype
  target._mixedin_ = $.extend true, {}, src
  target._mixedin_._bind_ = (that)->
    for key, val of target._mixedin_
      if _.isFunction(val)
        target._mixedin_[key] = _.bind(val, that) 

  #_.defaults(target.events, src.events) #if @ instanceof Backbone.View

  if src.methodsToReplace
    for m in src.methodsToReplace
      target[m] = src[m]
 
b = Backbone
b.Model.mixin = b.Collection.mixin = b.View.mixin = M3fe.mixin
