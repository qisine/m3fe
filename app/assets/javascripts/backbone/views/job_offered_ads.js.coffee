M3fe.Views.JobOfferedAds = Backbone.View.extend
  viewType: "JobOfferedAdsIndex"
  templ: window.JST['backbone/templates/job_offered_ads/index']
  events: {}

  initialize: ->
    @_mixedin_._bind_(@)
    @collection = @options.collection = (@options.collection || new M3fe.Collections.JobOfferedAds)
    @_mixedin_.initialize()
    _.bindAll(this, "render")
    @collection.on "reset", @render
    @collection.fetch
      success: ->
      error: ->

  insertIntoDOM: ->
    @_mixedin_.insertIntoDOM()
    $("#offered-tab").append(@$el)
  
  render: ->
    @_mixedin_.render()
    @$el.html(@templ({ collection: @collection }));

  onClose: ->
    @_mixedin_.onClose()

M3fe.Views.JobOfferedAds.mixin(M3fe.Views.Paginatable)
