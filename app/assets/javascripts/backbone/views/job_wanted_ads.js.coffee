M3fe.Views.JobWantedAds = Backbone.View.extend
  viewType: "JobWantedAdsIndex"
  templ: window.JST['backbone/templates/job_wanted_ads/index']
  events: {}
  initialize: ->
  
  render: ->
    @el.html(templ(null));

M3fe.Views.JobWantedAds.mixin(M3fe.Views.Paginatable)
