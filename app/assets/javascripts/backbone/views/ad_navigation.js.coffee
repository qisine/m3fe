M3fe.Views.AdNavigation = Backbone.View.extend
  viewType: "AdNavigation"
  templ: window.JST['backbone/templates/shared/ad_navigation']
  events:
    "click #ad-navigation > div": "switchTab"
  
  initialize: ->

  insertIntoDOM: ->
    $("#m3fe-app").prepend(@$el)

  switchTab: ->

  render: ->
    @$el.html(@templ());
  
  onClose: ->
    @undelegateEvents()
