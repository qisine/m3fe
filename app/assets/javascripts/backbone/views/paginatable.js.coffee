M3fe.Views.Paginatable =
  methodsToReplace: ["goTo", "next", "previous"]
  templ: window.JST["backbone/templates/shared/paginatable"]
  events:
    "click a#p_next": "next"
    "click a#p_previous": "previous"
    "change select#p_selection": "goTo"

  initialize: ->
    #_.bindAll(@, "next", "previous", "render")
    @_mixedin_.$el = $("<div></div>")
    @_mixedin_.cid = _.uniqueId('c')
    Backbone.View.prototype.delegateEvents.call(@_mixedin_)
    @collection = @options.collection
    page = parseInt(@options.page, 10) || 1
    page = 1 if page < 1
    @collection.page = page
    #@collection._pag_params = 
    #  search_string: $.trim(@options.search_string)

  insertIntoDOM: ->
    $("#ad-navigation").append(@_mixedin_.$el)
    
  render: ->
    t = @_mixedin_.templ({ info: @collection.pageInfo() })
    @_mixedin_.$el.html(t)
    
  goTo: (ev) ->
    ev.preventDefault();
    @collection.goTo(parseInt($(ev.target).attr("value"), 10) || -1)

  next: (ev) ->
    @collection.nextPage()
    return false

  previous: (ev) ->
    @collection.previousPage()
    return false

  onClose: ->
    @_mixedin_.$el.remove()
    #@undelegateEvents()
    #@collection.off
    #@off()
    #@remove()

  undelegateEvents: ->
    Backbone.View.prototype.undelegateEvents.call(@_mixedin_)
