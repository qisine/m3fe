class UserAbility
  include CanCan::Ability

  def initialize(user)
    guest_permissions
    user_permissions(user) if user.persisted?
  end

  def guest_permissions
    can :read, [JobOfferedAd, JobWantedAd]
  end

  def user_permissions(u)
    can :manage, [JobOfferedAd, JobWantedAd], user: { hash_id: u.hash_id }
  end
end
