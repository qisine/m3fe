class JobOfferedAd < ActiveRecord::Base
  include AdExtensions
  ad include: :work_place

  attr_accessible :pay, :work_place

  belongs_to :user
  has_one :base_ad, as: :advertisable, class_name: Ad
  has_one :category, as: :categorizable

  delegate :body, :phone, :title, to: :base_ad

  accepts_nested_attributes_for :base_ad

  validates_associated :base_ad
end
