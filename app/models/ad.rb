class Ad < ActiveRecord::Base
  attr_accessible :body, :phone, :title

  belongs_to :advertisable, polymorphic: true

  validates :body, :title, presence: true
end
