class JobWantedAd < ActiveRecord::Base
  include AdExtensions
  ad include: :desired_location

  attr_accessible :attachment, :desired_location, :phone, :body, :title

  belongs_to :user
  has_one :base_ad, as: :advertisable, class_name: Ad
  has_one :category, as: :categorizable

  delegate :body, :phone, :title, to: :base_ad

  accepts_nested_attributes_for :base_ad

  validates_associated :base_ad

  mount_uploader :attachment, AttachmentUploader
end
