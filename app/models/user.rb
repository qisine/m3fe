class User < ActiveRecord::Base
  attr_accessible :default_locale, :email, :name

  has_many :job_offered_ads
  has_many :job_wanted_ads

  validates :email, :name, presence: true
  validates :email, format: { with: /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i }

  after_save :set_checksum_id

  def checksum_id=(cid)
    raise "read only attribute"
  end

  def ==(other)
    return unless other.try { |o| o.respond_to :checksum_id }
    this.checksum_id == other.checksum_id
  end

  protected
  def set_checksum_id
    return if checksum_id
    require 'digest'
    write_attribute(:checksum_id, Digest::SHA256.new.hexdigest(email + created_at.to_s))
    save!
    raise "could not set checksum_id" if !self.checksum_id
  end
end
