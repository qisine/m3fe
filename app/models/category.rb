class Category < ActiveRecord::Base
  attr_accessible :code, :locale_index

  belongs_to :categorizable, polymorphic: true
end
