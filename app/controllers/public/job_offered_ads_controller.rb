class Public::JobOfferedAdsController < PublicController
  respond_to :js, :json

  def index
    page = params[:page] || 0
    @ads = JobOfferedAd.page(page)
    respond_to do |f|
      f.json { render json: { page: page, total: JobOfferedAd.count, perPage: Settings.items_per_page, models: @ads }, include: :base_ad }
    end
  end

  def show
    @ad = JobOfferedAd.find params[:id]
    respond_with(@ad, include: :base_ad)
  end

  def create
    ad = JobOfferedAd.new(params[:ad])
    respond_to do |r|
      if(ad.save!)
        r.json { render json: ad, include: :base_ad }
      else
        r.json { render json: ad.errors, status: 500 } 
      end
    end
  end
end
