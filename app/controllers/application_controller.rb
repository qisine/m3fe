class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :current_user
  before_filter :require_login, only: [:update, :destroy]

  def current_ability
    @current_ability ||= UserAbility.new(@current_user)
  end

  def current_user
    @current_user ||= User.new(params[:user])
    @current_user.save! if @current_user.valid? and @current_user.new_record?
  end

  protected

  def require_login
    unless current_user.persisted?
      render nothing: true, status: 401
    end
  end
end
