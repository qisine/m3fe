class Users::JobOfferedAdsController < UsersController
  respond_to :json, :js

  def update
    ad = JobOfferedAd.find params[:id]
    respond_to do |r|
      if ad.update_attributes(params[:ad])
        r.json { render json: ad }
      else
        r.json { render json: ad.errors, status: 500 }
      end
    end
  end

  def destroy
    ad = JobOfferedAd.find params[:id]
    respond_to do |r|
      if ad.try :destroy
        r.json { render json: ad }
      else
        r.json { render json: ad, status: 500 }
      end
    end
  end
end
